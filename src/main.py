from random import shuffle

'''
A + 10 = BJ unless split aces or tens then counted as 21, can only split once
Double if first 2 cards equal 9, 10 or 11
Only original bet lost if dealer draws blackjack in both cases
Push on same card total or both have blackjack
'''
cardDisplay = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
das = True
dealerStand = 17
bjPays = 1.5
play = True;
numDecks = 6;


def shuffledDeck(numberOfDecks):
    deck = []
    cardType = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]

    for k in range(numberOfDecks):
        for item in cardType:
            for i in range(4):
                deck.append(item)

    shuffle(deck)
    shuffle(deck)
    shuffle(deck)

    return deck


def displayNumber(numberToDisplay):
    return cardDisplay[numberToDisplay - 1]


def initialMoney():
    userInput = input("how much money do you wish to play? ")
    try:
        val = int(userInput)
        if val > 0:
            return val
        else:
            initialMoney()
    except ValueError:
        initialMoney()


def numberOfPlayers():
    userInput = input("Number of players? ")
    try:
        val = int(userInput)
        if val > 0:
            return val
        else:
            numberOfPlayers()
    except ValueError:
        numberOfPlayers()


def initialBet():
    userInput = input("Bet size? ")
    try:
        val = int(userInput)
        if val > 5:
            return val
        else:
            initialBet()
    except ValueError:
        initialBet()


def hitOrStand():
    userInput = input("Hit or Stand? ")
    try:
        val = userInput
        if val == "H":
            return True
        elif val == "S":
            return False
        else:
            hitOrStand()
    except ValueError:
        hitOrStand()


def keepPlaying():
    userInput = input("Keep playing? ")
    try:
        val = userInput
        if val == "Y":
            return True
        elif val == "N":
            return False
        else:
            keepPlaying()
    except ValueError:
        keepPlaying()


money = [0, 0, 0, 0, 0, 0, 0]
while play:
    deck = shuffledDeck(numDecks)
    numPlayers = numberOfPlayers()
    betSize = initialBet()
    for i in range(numPlayers):
        while money[i] < betSize:
            print("Player " + str(i + 1) + ", ")
            money[i] += initialMoney()

    hands = []
    totals = []
    for k in range(numPlayers + 1):
        hands.append([])
        totals.append([])

    for i in range(2):
        for k in range(numPlayers):
            hands[k].append(deck[0])
            del deck[0]
        if i == 0:
            hands[numPlayers].append(deck[0])
            del deck[0]

    dealerCard = hands[numPlayers][0]
    print("Dealer card showing is, " + str(dealerCard))
    for i in range(numPlayers):
        playerText = str(i + 1) + " "
        handSoft = False
        total = sum(hands[i])
        print(hands[i])
        if 1 in hands[i]:
            handSoft = True
            total += 10
            print("Player " + str(i + 1) + ", hand soft total = " + str(total))
        else:
            print("Player " + str(i + 1) + ", hand hard total = " + str(total))

        hit = hitOrStand()
        while hit:
            print(deck[0])
            hands[i].append(deck[0])
            total += deck[0]
            del deck[0]

            if total > 21 & handSoft == True:
                handSoft = False
                total -= 10
                print("Player " + str(i + 1) + ", hand hard total = " + str(total))
            elif total > 21:
                print("Bust")
            else:
                print("Player " + str(i + 1) + ", hand hard total = " + str(total))

            if total < 21:
                hit = hitOrStand()
            else:
                hit = False

        if total > 21:
            print("Player " + str(i + 1) + ", bust, total = " + str(total))
        else:
            print("Player " + str(i + 1) + ", stand, total = " + str(total))
        totals[i] = total

################
    ''' THIS PART IS NOT WORKING
    dealerTotal = sum(hands[len(hands) - 1])
    if 1 in hands[len(hands) - 1]:
        dealerSoft = True
        dealerTotal += 10
    else:
        dealerSoft = False

    while dealerTotal < 17:
        deckValue = deck[0]
        tempTotal = dealerTotal + deckValue
        if tempTotal > 21 & dealerSoft:
            dealerSoft = False
            tempTotal -= 10
        dealerTotal = tempTotal
    '''
##################
    if dealerTotal > 21:
        totals[len(totals) - 1] = 0
    else:
        if dealerTotal == 21 & len(hands[len(hands) - 1]) == 2:
            dealerBj = True
        else:
            dealerBj = False
        totals[len(totals) - 1] = dealerTotal

    for i in range(numPlayers + 1):
        if i < numPlayers:
            if totals[i] == 0:
                print("Player " + str(i + 1) + " has bust")
            else:
                print("Player " + str(i + 1) + " has total of " + str(totals[i]))
        else:
            print("Dealer has total of " + str(dealerTotal))

    for i in range(numPlayers):
        if totals[i] > 21:
            money[i] -= betSize
        elif dealerTotal == 0:
            money[i] += betSize
        elif totals[i] > dealerTotal:
            money[i] += betSize
        elif total[i] == dealerTotal:
            if len(hands[i]) > 2 & dealerBj:
                money[i] -= betSize
            elif len(hands[i]) == 2 & dealerBj == False:
                money[i] += betSize
        elif total[i] < dealerTotal:
            money[i] -= betSize

    for i in range(numPlayers):
        print("Player " + str(i + 1) + " has $" + str(money[i]))

    play = keepPlaying()